package tnt

import (
	"context"
	"fmt"
	"io"
	"os"

	nmea "bitbucket.org/mfkenney/go-nmea"
)

type SentenceType uint

const (
	HDG SentenceType = 0x07
	HDT              = 0x08
	XDR              = 0x09
	HTM              = 0x0a
	RCD              = 0x0b
	CCD              = 0x0c
	NCD              = 0x0d
	BIN              = 0x17
)

type Device struct {
	r io.ReadWriter
}

func NewDevice(r io.ReadWriter) *Device {
	return &Device{r: r}
}

func (d *Device) getRecord() ([]byte, error) {
	char := make([]byte, 1)
	rec := make([]byte, 0, 128)

	const startChar = '$'
	const endChar = '\n'

	for {
		n, err := d.r.Read(char)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}
		if err != nil {
			return nil, fmt.Errorf("looking for START: %w", err)
		}
		if char[0] == startChar {
			rec = append(rec, char[0])
			break
		}
	}

	for {
		n, err := d.r.Read(char)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}
		if err != nil {
			return nil, fmt.Errorf("looking for END: %w", err)
		}

		rec = append(rec, char[0])
		if char[0] == endChar {
			break
		}
	}

	return rec, nil
}

func (d *Device) EnableOutput() error {
	client := NewClient(d.r)
	return client.SetFlag(Flag{Addr: 0x13, Bit: 1})
}

func (d *Device) SetRate(st SentenceType, rate DataRate) error {
	client := NewClient(d.r)
	return client.WriteByte(uint(st), uint8(rate))
}

func (d *Device) Stream(ctx context.Context) (chan nmea.Sentence, chan error) {
	ch := make(chan nmea.Sentence, 1)
	err_ch := make(chan error, 1)

	go func() {
		defer close(ch)
		defer close(err_ch)
		for {
			select {
			case <-ctx.Done():
				err_ch <- ctx.Err()
				return
			default:
			}

			s, err := nmea.ReadSentence(d.r)
			if err != nil {
				err_ch <- err
				return
			}

			select {
			case ch <- s:
			default:
			}
		}
	}()

	return ch, err_ch
}
