// Package tnt provides an interface to True North Technologies electronic compasses.
package tnt

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
)

type BaudRate int

const (
	B2400  BaudRate = 1
	B4800           = 2
	B9600           = 3
	B19200          = 4
	B38400          = 5
	B57600          = 6
)

// RUN mode data rates in records per minute
type DataRate int

const (
	RateOff      DataRate = 0
	Rate1Pmin             = 1
	Rate2Pmin             = 2
	Rate3Pmin             = 3
	Rate6Pmin             = 4
	Rate12Pmin            = 5
	Rate20Pmin            = 6
	Rate30Pmin            = 7
	Rate60Pmin            = 8
	Rate120Pmin           = 9
	Rate180Pmin           = 10
	Rate275Pmin           = 11
	Rate413Pmin           = 12
	Rate550Pmin           = 13
	Rate825Pmin           = 14
	Rate206Pmin           = 16
	Rate118Pmin           = 17
	Rate31Pmin            = 19
	Rate15Pmin            = 20
	Rate8Pmin             = 21
	Rate4Pmin             = 22
	Rate1650Pmin          = 25
)

var (
	ErrAccessType = errors.New("Bad access type field")
	ErrSyntax     = errors.New("Syntax error")
	ErrAddress    = errors.New("Invalid address")
	ErrFlag       = errors.New("Invalid flag")
	ErrLen        = errors.New("Bad data length")
	ErrWrite      = errors.New("Invalid write")
	ErrContents   = errors.New("Invalid contents")
	ErrMem        = errors.New("Memory write error")
	ErrCmd        = errors.New("Invalid command")
	ErrTerm       = errors.New("Missing command terminator")
	ErrFraming    = errors.New("Framing error")
	ErrCsum       = errors.New("Bad checksum")
)

type Flag struct {
	Addr, Bit uint
}

// Client implements the TNT Command Protocol over an io.ReadWriter
type Client struct {
	rw io.ReadWriter
}

func NewClient(rw io.ReadWriter) *Client {
	return &Client{rw: rw}
}

func (c *Client) sendCmd(format string, a ...interface{}) error {
	csum := byte(0)
	var buf bytes.Buffer
	_, err := fmt.Fprintf(&buf, format, a...)
	if err != nil {
		return err
	}

	for _, b := range buf.Bytes() {
		csum = csum ^ b
	}
	c.rw.Write([]byte("@"))
	c.rw.Write(buf.Bytes())
	_, err = fmt.Fprintf(c.rw, "*%02X\r\n", csum)

	return err
}

func (c *Client) getResp() ([]byte, error) {
	char := make([]byte, 1)
	rec := make([]byte, 0, 128)
	bcsum := make([]byte, 0, 2)

	const startChar = '@'
	const endChar = '\n'

	for {
		n, err := c.rw.Read(char)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}
		if err != nil {
			return nil, fmt.Errorf("looking for START: %w", err)
		}
		if char[0] == startChar {
			break
		}
	}

	csum := byte(0)
	done := false

	for {
		n, err := c.rw.Read(char)
		if n == 0 {
			err = os.ErrDeadlineExceeded
		}
		if err != nil {
			return nil, fmt.Errorf("looking for END: %w", err)
		}

		if char[0] == endChar {
			break
		}

		if char[0] == '*' {
			done = true
			continue
		}

		if done {
			bcsum = append(bcsum, char[0])
		} else {
			rec = append(rec, char[0])
			csum = csum ^ char[0]
		}
	}

	sentCsum, err := strconv.ParseUint(string(bcsum[0:2]), 16, 8)
	if err != nil {
		return rec, fmt.Errorf("Bad checksum syntax: %w", err)
	}

	if uint8(sentCsum) != csum {
		err = ErrCsum
	}

	return rec, err
}

func parseError(resp []byte) error {
	switch val := string(resp[1:3]); val {
	case "00":
		return nil
	case "F1":
		return ErrAccessType
	case "F2":
		return ErrSyntax
	case "F3":
		return ErrAddress
	case "F4":
		return ErrFlag
	case "F5":
		return ErrLen
	case "F6":
		return ErrWrite
	case "F7":
		return ErrContents
	case "E8":
		return ErrMem
	case "80":
		return ErrCmd
	case "81":
		return ErrTerm
	case "82":
		return ErrFraming
	default:
		return fmt.Errorf("Unknown error code: %q", val)
	}
}

// ReadFlag reads a flag and returns the value as a bool
func (c *Client) ReadFlag(f Flag) (bool, error) {
	c.sendCmd("F%X.%d?", f.Addr, f.Bit)
	resp, err := c.getResp()
	if err != nil {
		return false, err
	}

	return resp[0] == '1', nil
}

func (c *Client) SetFlag(f Flag) error {
	c.sendCmd("F%X.%d=1", f.Addr, f.Bit)
	resp, err := c.getResp()
	if err != nil {
		return err
	}
	return parseError(resp)
}

func (c *Client) ClearFlag(f Flag) error {
	c.sendCmd("F%X.%d=0", f.Addr, f.Bit)
	resp, err := c.getResp()
	if err != nil {
		return err
	}
	return parseError(resp)
}

// ReadByte returns the unsigned 8-bit value at addr
func (c *Client) ReadByte(addr uint) (uint8, error) {
	c.sendCmd("B%X?", addr)
	resp, err := c.getResp()
	if err != nil {
		return 0, err
	}
	val, err := strconv.ParseUint(string(resp), 16, 8)
	return uint8(val), err
}

// WriteByte writes an unsigned 8-bit value to addr
func (c *Client) WriteByte(addr uint, val uint8) error {
	c.sendCmd("B%X=%XH", addr, val)
	resp, err := c.getResp()
	if err != nil {
		return err
	}
	return parseError(resp)
}

func (c *Client) ReadChar(addr uint) (int8, error) {
	c.sendCmd("C%X?", addr)
	resp, err := c.getResp()
	if err != nil {
		return 0, err
	}
	val, err := strconv.ParseInt(string(resp), 16, 8)
	return int8(val), err
}

func (c *Client) WriteChar(addr uint, val int8) error {
	c.sendCmd("C%X=%XH", addr, val)
	resp, err := c.getResp()
	if err != nil {
		return err
	}
	return parseError(resp)
}

func (c *Client) ReadWord(addr uint) (uint16, error) {
	c.sendCmd("W%X?", addr)
	resp, err := c.getResp()
	if err != nil {
		return 0, err
	}
	val, err := strconv.ParseUint(string(resp), 16, 16)
	return uint16(val), err
}

func (c *Client) WriteWord(addr uint, val uint16) error {
	c.sendCmd("W%X=%XH", addr, val)
	resp, err := c.getResp()
	if err != nil {
		return err
	}
	return parseError(resp)
}

func (c *Client) ReadInt(addr uint) (int16, error) {
	c.sendCmd("I%X?", addr)
	resp, err := c.getResp()
	if err != nil {
		return 0, err
	}
	val, err := strconv.ParseInt(string(resp), 16, 16)
	return int16(val), err
}

func (c *Client) WriteInt(addr uint, val int16) error {
	c.sendCmd("I%X=%XH", addr, val)
	resp, err := c.getResp()
	if err != nil {
		return err
	}
	return parseError(resp)
}
