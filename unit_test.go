package tnt

import (
	"bytes"
	"testing"
)

func TestFlagRead(t *testing.T) {
	table := []struct {
		f          Flag
		send, recv string
		val        bool
	}{
		{
			f:    Flag{Addr: 0x13, Bit: 1},
			send: "@F13.1?*64\r\n",
			recv: "@1*31\r\n",
			val:  true,
		},
		{
			f:    Flag{Addr: 1, Bit: 6},
			send: "@F1.6?*50\r\n",
			recv: "@0*30\r\n",
			val:  false,
		},
	}

	var b bytes.Buffer
	client := NewClient(&b)
	for _, e := range table {
		b.Reset()
		b.Write([]byte(e.recv))
		val, err := client.ReadFlag(e.f)
		if err != nil {
			t.Fatal(err)
		}

		if val != e.val {
			t.Errorf("Bad response; expected %v, got %v", e.val, val)
		}

		if s := string(b.Bytes()); s != e.send {
			t.Errorf("Bad command; expected %q, got %q", e.send, s)
		}
	}
}

func TestFlagClear(t *testing.T) {
	table := []struct {
		f          Flag
		send, recv string
		err        error
	}{
		{
			f:    Flag{Addr: 0x28, Bit: 5},
			send: "@F28.5=0*5A\r\n",
			recv: "@!0000*21\r\n",
			err:  nil,
		},
		{
			f:    Flag{Addr: 0x28, Bit: 8},
			send: "@F28.8=0*57\r\n",
			recv: "@!F400*53\r\n",
			err:  ErrFlag,
		},
	}

	var b bytes.Buffer
	client := NewClient(&b)
	for _, e := range table {
		b.Reset()
		b.Write([]byte(e.recv))
		err := client.ClearFlag(e.f)
		if err != e.err {
			t.Errorf("Unexpected error value: %v", err)
		}
		if s := string(b.Bytes()); s != e.send {
			t.Errorf("Bad command; expected %q, got %q", e.send, s)
		}
	}
}

func TestFlagSet(t *testing.T) {
	table := []struct {
		f          Flag
		send, recv string
		err        error
	}{
		{
			f:    Flag{Addr: 0x28, Bit: 5},
			send: "@F28.5=1*5B\r\n",
			recv: "@!0000*21\r\n",
			err:  nil,
		},
		{
			f:    Flag{Addr: 0x28, Bit: 8},
			send: "@F28.8=1*56\r\n",
			recv: "@!F400*53\r\n",
			err:  ErrFlag,
		},
	}

	var b bytes.Buffer
	client := NewClient(&b)
	for _, e := range table {
		b.Reset()
		b.Write([]byte(e.recv))
		err := client.SetFlag(e.f)
		if err != e.err {
			t.Errorf("Unexpected error value: %v", err)
		}
		if s := string(b.Bytes()); s != e.send {
			t.Errorf("Bad command; expected %q, got %q", e.send, s)
		}
	}
}

func TestByteRead(t *testing.T) {
	table := []struct {
		addr       uint
		send, recv string
		val        uint8
	}{
		{
			addr: 6,
			send: "@B6?*4B\r\n",
			recv: "@4*34\r\n",
			val:  4,
		},
		{
			addr: 0x0c,
			send: "@BC?*3E\r\n",
			recv: "@0*30\r\n",
			val:  0,
		},
		{
			addr: 0x17,
			send: "@B17?*7B\r\n",
			recv: "@42*06\r\n",
			val:  0x42,
		},
	}

	var b bytes.Buffer
	client := NewClient(&b)
	for _, e := range table {
		b.Reset()
		b.Write([]byte(e.recv))
		val, err := client.ReadByte(e.addr)
		if err != nil {
			t.Fatal(err)
		}

		if val != e.val {
			t.Errorf("Bad response; expected %v, got %v", e.val, val)
		}

		if s := string(b.Bytes()); s != e.send {
			t.Errorf("Bad command; expected %q, got %q", e.send, s)
		}
	}
}
